F2/Cmd
==========

**Cmd** is a simple command line parser for PHP, to make it easier to create command line tools
with PHP.

Very simple example
-------------------

```php
#!/usr/bin/env php
<?php
require('vendor/autoload.php');

$cmd = new F2\Cmd\Cmd("Example tool", [
    'a|alpha'   => 'This is the description of the alpha parameter',
    '|beta'     => 'The beta parameter',
    'c'         => 'The charlie parameter',
]);

if ($cmd->flag('a')) {
    die("ALPHA!\n");
} else if ($cmd->flag('beta')) {
    die("BETA! VALUE OF c=".json_encode($cmd->value('c'))."\n");
} else if ($cmd->flag('c')) {
    die("VALUE OF c=".json_encode($cmd->value('c'))."\n");
}

echo "ALL OK!\n";

```

Usage:

```
# ./simple-example
ALL OK!
# ./simple-example --undeclared-option
simple-example: unrecognized option '--undeclared-option'
Try 'simple-example --help' for more information.
# ./simple-example -a
ALPHA!
# ./simple-example --alpha
ALPHA!
# ./simple-example --beta
BETA! VALUE OF c=null
# ./simple-example -c
VALUE OF c=true
# ./simple-example -c whatever
VALUE OF c='whatever'
```


Installation
------------

The recommended way to install Fubber CMD is throught [Composer](http://getcomposer.org/)

```bash
$ composer require f2/cmd
```


API
---

### Constructor

```php
Cmd::__construct(string $usage, array $args=[], array $mandatoryParams=[]);
```

**$usage** is a short description of your utility. You can use multiple lines if your
utility warrants it.

**$args** is an array of arguments with a usage description. The key contains a pipe
separated list of arguments, where the first part is a group of short options: 
'abc|long1|long2'. For each short option and each long option you may specify a colon 
(:) to make the option require a value, or a double colon (::) to optionally have a value.

Format of the array keys:

```php
[
    'a:b:c:|long1:|long2:|long3:' => 'Three short and three long options that require a value',
    'a:b::|long1:|long2::' => 'A combination of required and optional value',
    '|no-short-option' => 'No short option available',
    ]
```

More examples:

```php
$args = [
    'a' => 'The -a flag is boolean and can be checked with $cmd->flag('a')',
    'b:' => 'Requires a value: -b 20. Check with $cmd->value('b').'
    'd:e:' => 'Aliases for a value: -d 20 is equivalent to -e 20.',
    'f:|force:' => 'Short and long version -f 20 and --force=20.',
    '|no-short' => 'No short version, only long flag --no-short.',
    ];
```

**$mandatoryParams** is a list of mandatory parameters at the end of your command:

```php
$mandatoryParams = [
    'what-to-say',
    ];
```

Will make the following output:

```bash
$ your-tool --help
your-tool <what-to-say>
```

### Flags

Flags are boolean options. They come in two forms: short and long.

```php
$cmd = new Cmd('Tool with flags', [
    'abc' => 'Flags -a, -b and -c',                 // -a, -b and -c are aliases now
    'd|the-d-flag' => 'Flags -d and --the-d-flag',  // -d and --the-d-flag are aliases
]);
$a = $cmd->flag('a');
$b = $cmd->flag('a');
$theDFlag = $cmd->flag('d');
```

### Options

Options are like flags, but they have a mandatory value. This is enabled by adding a colon (:) to the definition.

```php
$cmd = new Cmd('Tool with options', [
    'a:b:c' => 'Option -a <value>, -b <value> and -c <value>',
    '|the-d-option' => 'Option --the-d-option=<value>',
    'e|the-e-option|the-extended-option' => 'Option -e <value>, --the-e-option=<value>, --the-extended-option=<value>',
]);
$a = $cmd->value('a');
$b = $cmd->value('b');
$c = $cmd->value('c');
```

### Flags with optional value

Flags with optional options are marked with a double colon (::) in the definition.

```php
$cmd = new Cmd('Tool with options', [
    'a::' => 'Option -a [value]',
    '|long-optional-option::' => 'Optional option',
]);
```

### Arguments

Arguments are required arguments to the function.

```php
$cmd = new Cmd('Tool with arguments', null, ['argument']);
```

Arguments are available through the built in PHP global $argv. Also
you can access a version of $argv where the defined flags and options
have been removed:

```php
print_r( $cmd->argv );
```


More
----

You must do more advanced validation yourself. If there are validation
errors, simply explain the error using the Cmd::error method.

```php
$cmd->error("You can't combine -a with -b");
```

To display the command usage yourself:

```php
$cmd->usage();
die();
```

