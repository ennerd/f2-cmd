<?php
namespace F2\Cmd;

class Cmd {
	protected $optIndex;
	protected $args;
	protected $usage;
    protected $aliases = [];
    protected $extraParams;
    protected $settings;
	public $argv;

	/**
	* $cmd = new Cmd('Description', [
	*	'h|help' => 'Show help',        // -h and --help
    *   '|dry-run' => 'Dry run',        // --dry-run
    *   'h?|help|options' => Show help  // -h -? --help --options
	* ], [
    *   'filename'                      // <filename> as a mandatory parameter
    * ]);
	*/
	public function __construct($usage, array $args=[], array $extraParams=[], array $settings=[]) {
		global $argv;

        $this->settings = $settings + [
            // Optional in-depth description below the argument list
            'description' => null,
        ];


		$this->usage = $usage;
		$this->args = $args;
		$this->args['h|help'] = 'Show this help';
        $this->extraParams = $extraParams;

		$short = '';
		$long = [];
		foreach($this->args as $args => $desc) {
			$p = explode("|", $args);
            for ($i = 0; $i < strlen($p[0]); $i++) {
                if ($p[0][$i]==":") {
                    continue;
                }
                $this->aliases[$p[0][$i]] = $args;
            }
			if(trim($p[0])!=='') {
                $illegal = trim($p[0], 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:');
                if ($illegal !== '') {
                    throw new IllegalOptionException("Illegal character '".$illegal[0]."' for options");
                }
				$short .= $p[0];
			}
			for($i = 1; $i < sizeof($p); $i++) {
                $this->aliases[rtrim($p[$i], ":")] = $args;
				$long[] = $p[$i];
			}
		}
		$offset = 0;
		$opts = getopt($short, $long, $offset);

/*
		$offsetShouldBe = 1 + sizeof($opts);
		if($offsetShouldBe != $offset) {
			$this->error('unrecognized option \''.$argv[$offsetShouldBe].'\'');
			die(1);
		}
*/

		$this->argv = [$argv[0]];
		for($i = $offset; $i < sizeof($argv); $i++) {
			$this->argv[] = $argv[$i];
		}

		if($this->flag('h')) {
			$this->usage();
			die();
		}
	}

	/**
    * Checks argument list against options, for example ab|charlie|delta
    * will return true if -a, -b --charlie or --delta is specified.
    *
	* if($cmd->flag('h|help')) {
	* 	// show help
	* }
	*/
	public function flag($f) {
        list($short, $long) = $this->getSpec($f);
		return sizeof(getopt($short, $long)) > 0;
	}

    public function value($v) {
        list($short, $long) = $this->getSpec($v);
        $optind = null;
        $res = getopt($short, $long, $optind);

        if (sizeof($res)===0) {
            return null;
        }

        $result = [];
        foreach($res as $k => $v) {
            if (is_array($v)) {
                foreach($v as $v2) {
                    $result[] = $v2;
                }
            } else {
                $result[] = $v;
            }
        }
        if (sizeof($result)===0) {
            return null;
        }
        return $result;
    }

    protected function getSpec(string $key) {
        if (!isset($this->aliases[$key])) {
            if (sizeof(explode("|", $key))>1) {
                throw new UnknownOptionException("Query a single option only ($key). F2\Cmd will automatically check aliases.");
            }
            throw new UnknownOptionException("The option '$key' has not been declared.");
        }
        $hash = md5(serialize($this->aliases[$key]));
        $parts = explode("|", $this->aliases[$key]);
        $short = array_shift($parts);
        return [$short, $parts, $hash];
    }

	public function error($message) {
		global $argv;
		fwrite(STDERR, basename($argv[0]).': '.$message."\n");
		fwrite(STDERR, 'Try \''.basename($argv[0]).' --help\' for more information.'."\n");
	}

	public function usage() {
		global $argv;
        if (sizeof($this->extraParams)>0) {
            echo "Usage: ".basename($argv[0])." <".implode("> <", $this->extraParams).">\n";
        } else {
    		echo "Usage: ".basename($argv[0])."\n";
        }
		echo trim($this->usage)."\n";
		if(sizeof($this->args)>0) {
			$lines = [];
			foreach($this->args as $k => $desc) {
				$parts = explode("|", $k);
				$opts = [];
				// short opts
				for($i = 0; $i < strlen($parts[0]); $i++) {
					$char = $parts[0][$i];
					if($char != ':') {
						$opts[] = '-'.$char;
                    }
				}
				for($i = 1; $i < sizeof($parts); $i++) {
					if(($trimmed = rtrim($parts[$i], ':')) != $parts[$i]) {
						$opts[] = '--'.$trimmed.'=VAL';
					} else {
						$opts[] = '--'.$parts[$i];
					}
				}
				$lines[] = [implode(", ", $opts), $desc];
			}
			$maxWidth = 0;
			foreach($lines as $line) {
				if(strlen($line[0])>$maxWidth)
					$maxWidth = strlen($line[0]);
			}
			echo "\n";
			foreach($lines as $line) {
				echo str_pad($line[0], $maxWidth)."\t".$line[1]."\n";
			}

            if ($this->settings['description']) {
                $columns = static::getCols();
                echo "\n\n".wordwrap($this->settings['description'], $columns)."\n\n";
            }
		}
	}

    /**
     * Get the number of columns for the viewport
     */
    public static function getCols(): int {
        return min(75, intval(exec('tput cols') ?? 75));
    }
}
